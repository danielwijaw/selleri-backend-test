'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Product = use("App/Models/Selleri/Product");
const CategoryProduct = use("App/Models/Selleri/CategoryProduct");

class Cart extends Model {
    static get incrementing () {
        return true
    }
    
    static get table () {
      return 'carts'
    }
  
    static get primaryKey () {
      return 'carts_id'
    }

    static get searchField (){
      return 'carts_consumen_name'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at", "carts_category_product_history", "carts_product_history"];
    }

    static get relations(){
        return ["categoryProducts", "products"]
    }

    static fillable(){
        return {
            created: [
                // "carts_category_product_id",
                "carts_product_id",
                "carts_consumen_name"
            ],
            updated: [
                // "carts_category_product_id",
                "carts_product_id",
                "carts_consumen_name"
            ],
        }
    }

    static validator(){
        return {
            created: {
                carts_consumen_name: "required|max:150",
                carts_product_id: "required|uuid|relations:products,products_id",
                // carts_category_product_id: "required|uuid|relations:category_products,category_products_id"
            },
            updated: {
                carts_consumen_name: "required|max:150",
                carts_product_id: "required|uuid|relations:products,products_id",
                // carts_category_product_id: "required|uuid|relations:category_products,category_products_id"
            },
        }
    }

    categoryProducts(){
        return this.belongsTo('App/Models/Selleri/CategoryProduct', 'carts_category_product_id', 'category_products_id')
    }

    products(){
        return this.belongsTo('App/Models/Selleri/Product', 'carts_product_id', 'products_id')
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')

        this.addHook('beforeCreate', async (userInstance) => {

            const getProducts = await Product.query().where(Product.primaryKey, userInstance.carts_product_id).first()
            if(!getProducts){
                throw new Error('Products Not Found')
            }

            const getCategoryProduct = await CategoryProduct.query().where(CategoryProduct.primaryKey, getProducts.products_category_id).first()
            if(!getCategoryProduct){
                throw new Error('Category Products Not Found')
            }

            userInstance.carts_category_product_history = getCategoryProduct.toJSON()
            userInstance.carts_product_history = getProducts.toJSON()
            userInstance.carts_price = getProducts.products_price
            userInstance.carts_category_product_id = getProducts.products_category_id

        })
    }
}

module.exports = Cart
