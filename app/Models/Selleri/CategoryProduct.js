'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CategoryProduct extends Model {
    static get incrementing () {
        return true
    }
    
    static get table () {
      return 'category_products'
    }
  
    static get primaryKey () {
      return 'category_products_id'
    }

    static get searchField (){
      return 'category_products_name'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at"];
    }

    static fillable(){
        return {
            created: [
                "category_products_name"
            ],
            updated: [
                "category_products_name"
            ],
        }
    }

    static validator(){
        return {
            created: {
                category_products_name: "required|max:120|exists:category_products,category_products_name"
            },
            updated: {
                category_products_name: "required|max:120|exists:category_products,category_products_name"
            },
        }
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
    }
}

module.exports = CategoryProduct
