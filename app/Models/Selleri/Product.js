'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
    static get incrementing () {
        return true
    }
    
    static get table () {
      return 'products'
    }
  
    static get primaryKey () {
      return 'products_id'
    }

    static get searchField (){
      return 'products_name'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at"];
    }

    static get relations(){
        return ["categoryProducts"]
    }

    static fillable(){
        return {
            created: [
                "products_name",
                "products_price",
                "products_location",
                "products_detail",
                "products_category_id"
            ],
            updated: [
                "products_name",
                "products_price",
                "products_location",
                "products_detail",
                "products_category_id"
            ],
        }
    }

    static validator(){
        return {
            created: {
                products_name: "required|max:120",
                products_price: "required",
                products_location: "required|max:120",
                products_detail: "required",
                products_category_id: "required|uuid|relations:category_products,category_products_id"
            },
            updated: {
                products_name: "required|max:120",
                products_price: "required",
                products_location: "required|max:120",
                products_detail: "required",
                products_category_id: "required|uuid|relations:category_products,category_products_id"
            },
        }
    }

    categoryProducts(){
        return this.belongsTo('App/Models/Selleri/CategoryProduct', 'products_category_id', 'category_products_id')
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
    }
}

module.exports = Product
