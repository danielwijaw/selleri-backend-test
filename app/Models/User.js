'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static get incrementing () {
    return true
  }

  static get table () {
    return 'users'
  }

  static get primaryKey () {
    return 'users_id'
  }

  static get hidden() {
    return ["updated_at", "deleted_at"];
    // return ["users_password", "created_at", "updated_at", "deleted_at", "users_id"];
  }

  static boot () {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.users_password) {
        userInstance.users_password = await Hash.make(userInstance.users_password)
      }
    })
  }
  
  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }
}

module.exports = User
