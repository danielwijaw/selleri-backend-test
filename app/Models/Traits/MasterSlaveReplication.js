'use strict'

class MasterSlaveReplication {
  register (Model) {

    const thisConnection = Model.connection;

    Object.defineProperty(Model, 'connection', {
      get: () => {
        return this.connection
      },
      set: connection => {
        this.connection = connection
      }
    })

    Model.addHook('beforeCreate', () => {
      this.connection = thisConnection.replace('_slave','')
    })
    Model.addHook('beforeUpdate', () => {
      this.connection = thisConnection.replace('_slave','')
    })
    Model.addHook('beforeSave', () => {
      this.connection = thisConnection.replace('_slave','')
    })
    Model.addHook('beforeDelete', () => {
      this.connection = thisConnection.replace('_slave','')
    })

    Model.addHook('afterCreate', () => {
      this.connection = (thisConnection.match('_slave') ? thisConnection : thisConnection+"_slave")
    })
    Model.addHook('afterUpdate', () => {
      this.connection = (thisConnection.match('_slave') ? thisConnection : thisConnection+"_slave")
    })
    Model.addHook('afterSave', () => {
      this.connection = (thisConnection.match('_slave') ? thisConnection : thisConnection+"_slave")
    })
    Model.addHook('afterDelete', () => {
      this.connection = (thisConnection.match('_slave') ? thisConnection : thisConnection+"_slave")
    })
    Model.addHook('afterFind', () => {
      this.connection = (thisConnection.match('_slave') ? thisConnection : thisConnection+"_slave")
    })
    Model.addHook('afterFetch', () => {
      this.connection = (thisConnection.match('_slave') ? thisConnection : thisConnection+"_slave")
    })
    Model.addHook('afterPaginate', () => {
      this.connection = (thisConnection.match('_slave') ? thisConnection : thisConnection+"_slave")
    })
  }
}

module.exports = MasterSlaveReplication
