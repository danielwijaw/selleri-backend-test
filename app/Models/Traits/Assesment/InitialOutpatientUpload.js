'use strict'

class InitialOutpatientUpload {
  register (Model, customOptions = {}) {
    const defaultOptions = {
      fieldDatabase: "upload_file_patients_keyword_relations",
      valueDatabase: "assesment_initial_outpatients"
    }
    const options = Object.assign(defaultOptions, customOptions)

    const fieldDatabase = options.fieldDatabase
    const valueSearch = options.valueDatabase


    Model.addGlobalScope(
      function (query) {
        if(valueSearch == 'null'){
          query.whereNull(fieldDatabase)
        }else if(valueSearch == 'not null'){
          query.whereNotNull(fieldDatabase)
        }else{
          query.where(fieldDatabase, valueSearch)
        }
      }
    )
  }
}

module.exports = InitialOutpatientUpload
