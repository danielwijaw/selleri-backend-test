'use strict'

class InitialOutpatientUploadNot {
  register (Model, customOptions = {}) {
    const defaultOptions = {
      fieldDatabase: "upload_file_patients_keyword_relations",
      valueDatabase: "assesment_initial_outpatients"
    }
    const options = Object.assign(defaultOptions, customOptions)

    const fieldDatabase = options.fieldDatabase
    const valueSearch = options.valueDatabase

    Model.addGlobalScope(
      function (query) {
        query.where(fieldDatabase, '!=', valueSearch)
      }
    )
  }
}

module.exports = InitialOutpatientUploadNot
