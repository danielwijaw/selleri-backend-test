'use strict'

const Database = use('Database');
const EmployeeVacation = use("App/Models/Employee/EmployeeVacation");

class VacationDoctor {
  async register (Model, customOptions = {}) {
    const defaultOptions = {}
    const options = Object.assign(defaultOptions, customOptions)

    // Get Date This Week
    var getIntervalThisWeek = await Database.raw(`
    SELECT 
      date_trunc('week', current_date) - INTERVAL '0days' as Monday,
      date_trunc('week', current_date) + INTERVAL '1days' as Tuesday,
      date_trunc('week', current_date) + INTERVAL '2days' as Wednesday,
      date_trunc('week', current_date) + INTERVAL '3days' as Thursday,
      date_trunc('week', current_date) + INTERVAL '4days' as Friday,
      date_trunc('week', current_date) + INTERVAL '5days' as Saturday,
      date_trunc('week', current_date) + INTERVAL '6days' as Sunday
    where 1 = ?
    `, [1]);

    var getIntervalThisWeek = getIntervalThisWeek.rows[0]

    // Get Vacations Doctor
    var getVacation = await EmployeeVacation.query().whereRaw('is_approved = \'true\'').whereRaw('employee_vacations_end >= DATE(?) and employee_vacations_end <= DATE(?) ',[getIntervalThisWeek.monday.toISOString().slice(0,10), getIntervalThisWeek.sunday.toISOString().slice(0,10)]).fetch()
    var getVacation = getVacation.toJSON()

    for(var keysV in getVacation){
      getVacation[keysV].employee_vacations_start = getVacation[keysV].employee_vacations_start.toISOString().slice(0,10)
      getVacation[keysV].employee_vacations_end = getVacation[keysV].employee_vacations_end.toISOString().slice(0,10)
    }

    Model.addGlobalScope(
      function (query) {
        // query.whereRaw()
      }
    )
  }
}

module.exports = VacationDoctor
