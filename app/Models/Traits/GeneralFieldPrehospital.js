'use strict'

class GeneralFieldPrehospital {
  register (Model, customOptions = {}) {
    const defaultOptions = {
      fieldDatabase: "general_field_prehospitals_part",
      valueDatabase: "gcs"
    }
    const options = Object.assign(defaultOptions, customOptions)

    const fieldDatabase = options.fieldDatabase
    const valueSearch = options.valueDatabase

    Model.addGlobalScope(
      function (query) {
        query.where(fieldDatabase, valueSearch)
      }
    )
  }
}

module.exports = GeneralFieldPrehospital
