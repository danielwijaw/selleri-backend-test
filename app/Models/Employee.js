'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Redis = use('Redis');

class Employee extends Model {

    static get incrementing () {
        return true
    }
    
    static get table () {
      return 'employees'
    }
  
    static get primaryKey () {
      return 'employees_id'
    }

    static get searchField (){
      return 'employees_name'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at"];
    }

    static get relations(){
        return ["positions.positions", "users_account"]
    }

    static fillable(){
        return {
            created: [
                "employees_niprs",
                "employees_fingerprint_id",
                "employees_type",
                "employees_status_id",
                "employees_start",
                "employees_end",
                "employees_name",
                "employees_nickname",
                "employees_birthplace",
                "employees_birthdate",
                "employees_gender",
                "employees_religion",
                "employees_nation",
                "employees_marriage",
                "employees_phone",
                "employees_phone_number",
                "employees_telegram_id",
                "employees_email",
                "employees_hobbies",
                "employees_card_number",
                "employees_province_id",
                "employees_district_id",
                "employees_sub_district_id",
                "employees_village_id",
                "employees_address"
            ],
            updated: [
                "employees_niprs",
                "employees_fingerprint_id",
                "employees_type",
                "employees_status_id",
                "employees_start",
                "employees_end",
                "employees_name",
                "employees_nickname",
                "employees_birthplace",
                "employees_birthdate",
                "employees_gender",
                "employees_religion",
                "employees_nation",
                "employees_marriage",
                "employees_phone",
                "employees_phone_number",
                "employees_telegram_id",
                "employees_email",
                "employees_hobbies",
                "employees_card_number",
                "employees_province_id",
                "employees_district_id",
                "employees_sub_district_id",
                "employees_village_id",
                "employees_address"
            ],
        }
    }

    static validator(){
        return {
            created: {
                employees_niprs: "required|max:15|exists:employees,employees_niprs",
                employees_fingerprint_id: "max:5",
                employees_type: "required|max:15",
                employees_status_id: "required|uuid|relations:employee_statuses,employee_statuses_id",
                employees_start: "required|dateFormat:YYYY-MM-DD",
                employees_end: "date",
                employees_name: "required|max:150",
                employees_nickname: "required|max:25",
                employees_birthplace: "required|max:25",
                employees_birthdate: "required|dateFormat:YYYY-MM-DD",
                employees_gender: "required|max:12",
                employees_religion: "required|max:12",
                employees_nation: "required|max:30",
                employees_marriage: "required|max:12",
                employees_phone: "required|max:18",
                employees_phone_number: "max:18",
                employees_telegram_id: "max:25",
                employees_email: "email|max:50",
                employees_hobbies: "required|max:125",
                employees_card_number: "required|max:18|exists:employees,employees_card_number",
                employees_province_id: "required|max:2|relations:province,province_id",
                employees_district_id: "required|max:5|relations:district,district_id",
                employees_sub_district_id: "required|max:8|relations:sub_district,sub_district_id",
                employees_village_id: "required|max:13|relations:village,village_id",
                is_doctor: "in:true,false",
                is_nurse: "in:true,false"
            },
            updated: {
                employees_niprs: "required|max:15",
                // employees_fingerprint_id: "max:5",
                // employees_type: "required|max:15",
                // employees_status_id: "required|uuid|relations:employee_statuses,employee_statuses_id",
                // employees_start: "required|dateFormat:YYYY-MM-DD",
                // employees_end: "date",
                // employees_name: "required|max:150",
                // employees_nickname: "required|max:25",
                // employees_birthplace: "required|max:25",
                // employees_birthdate: "required|dateFormat:YYYY-MM-DD",
                // employees_gender: "required|max:12",
                // employees_religion: "required|max:12",
                // employees_nation: "required|max:30",
                // employees_marriage: "required|max:12",
                // employees_phone: "required|max:18",
                // employees_phone_number: "max:18",
                // employees_telegram_id: "max:25",
                // employees_email: "email|max:50",
                // employees_hobbies: "required|max:125",
                // employees_card_number: "required|max:18",
                // employees_province_id: "required|max:2|relations:province,province_id",
                // employees_district_id: "required|max:5|relations:district,district_id",
                // employees_sub_district_id: "required|max:8|relations:sub_district,sub_district_id",
                // employees_village_id: "required|max:13|relations:village,village_id",
                // is_doctor: "in:true,false",
                // is_nurse: "in:true,false"
            },
        }
    }

    users_account(){
        return this.hasMany('App/Models/User', 'employees_id', 'users_employees_id')
    }

    contracts(){
        return this.hasMany('App/Models/EmployeeContract', 'employees_id', 'employee_contracts_employees_id')
    }

    administrations(){
        return this.hasMany('App/Models/EmployeeAdministration', 'employees_id', 'employee_administrations_employees_id')
    }

    positions(){
        return this.hasMany('App/Models/EmployeePosition', 'employees_id', 'employee_positions_employees_id')
    }

    departmentsEmployee(){
        return this.hasMany('App/Models/EmployeeDepartment', 'employees_id', 'employee_departments_employees_id')
    }

    departmentsDetailEmployee(){
        return this.manyThrough('App/Models/EmployeeDepartment', 'departments', 'employees_id', 'employee_departments_employees_id')
    }

    positionsDetailEmployee(){
        return this.manyThrough('App/Models/EmployeePosition', 'positions', 'employees_id', 'employee_positions_employees_id')
    }

    linkProfilePicture(){
        const employeePict = this.hasOne('App/Models/Employee/ProfilePicture', 'employees_id', 'employee_media_employees_id')
        // return 'api/v1/employee-profile-picture/data/' + employeePict.employee_media_id
        return employeePict
    }

    scheduleDoctor(){
        return this.manyThrough('App/Models/DoctorSchedule', 'polyclinic', 'employees_id', 'doctor_schedules_employees_id')
    }

    static async getRowData(idRow){
        const keyNameRedis = 'Employee List Models'+idRow+' showrow';

        const storeDataRedis = await Redis.get(keyNameRedis);

        if (storeDataRedis) {
            return JSON.parse(storeDataRedis)
        }

        const EmployeeData =  await Employee.query().where(Employee.primaryKey, idRow).with('contracts').with('administrations').with('departmentsEmployee.departments.departmentTypes').with('positions.positions').fetch()

        await Redis.set(keyNameRedis, JSON.stringify(EmployeeData))

        return EmployeeData
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
    }
}

module.exports = Employee
