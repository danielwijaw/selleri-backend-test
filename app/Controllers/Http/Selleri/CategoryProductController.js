'use strict'

const CategoryProduct = use("App/Models/Selleri/CategoryProduct");
const ApiController = require("../../../../app/Library/ApiController");

class CategoryProductController extends ApiController {
    constructor() {
        super()
        this.modelData = CategoryProduct;
        this.nameModel = "Selleri Category Product List";
        this.relationsNameModel = this.nameModel;
    }
}

module.exports = CategoryProductController
