'use strict'

const Cart = use("App/Models/Selleri/Cart");
const ApiController = require("../../../../app/Library/ApiController");

class CartController extends ApiController {
    constructor() {
        super()
        this.modelData = Cart;
        this.nameModel = "Selleri Cart List";
        this.relationsNameModel = this.nameModel;
    }
}

module.exports = CartController
