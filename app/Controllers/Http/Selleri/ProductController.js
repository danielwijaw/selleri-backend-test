'use strict'

const Product = use("App/Models/Selleri/Product");
const ApiController = require("../../../../app/Library/ApiController");

class ProductController extends ApiController {
    constructor() {
        super()
        this.modelData = Product;
        this.nameModel = "Selleri Product List";
        this.relationsNameModel = this.nameModel;
    }
}

module.exports = ProductController
