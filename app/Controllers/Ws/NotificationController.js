'use strict'

class NotificationController {

  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
    console.log('A new subscription for notification', socket.topic)
  }

  onMessage (message) {
    this.socket.broadcastToAll('message', message)
    console.log('got message', message)
  }

  onClose () {
    console.log('Closing subscription for room topic', this.socket.topic)
  }
}

module.exports = NotificationController
