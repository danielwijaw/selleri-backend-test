'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ResponseHelper = require('./../Library/ResponseHelper')
const Redis = use('Redis')
const moment = require('moment')
const WriteTmpLog = require('./../Library/WriteTmpLog')

const deviceType = [
  'react-native',
  'react-frontend',
  'vue-profile',
  'development'
]

const WINDOW_SIZE_IN_HOURS = 0.1;
const MAX_WINDOW_REQUEST_COUNT = 100;

class UserAgentAllow {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, response }, next) {
    await next();
    return true

    const listRequestHeaders = request.headers()

    if( !deviceType.includes(listRequestHeaders['devicetype']) ){
      await WriteTmpLog({
        directoryLog: 'error/',
        nameLog: 'error-log',
        fileData: {
            date: new Date(),
            requestUrl: request.url(),
            userRequest: {
                ip: request.ip()+" || "+request.ips(),
                hostname: request.hostname(),
                originalUrl: request.originalUrl(),
                method: request.method(),
                header: listRequestHeaders,
                body: request.all()
            },
            errorMessage: 'Header deviceType wrong/empty',
            errorName: 'Headers Unauthorized',
            errorStack: 'Header deviceType wrong/empty'
        }
      })

      return response.status(401).json(
        ResponseHelper({
            responseCode: 401,
            status: "false",
            message: 'Header deviceType wrong/empty',
            error: {
                message: 'Header deviceType wrong/empty'
            }
        })
      )
    }

    if( !listRequestHeaders['deviceid'] ){
      await WriteTmpLog({
        directoryLog: 'error/',
        nameLog: 'error-log',
        fileData: {
            date: new Date(),
            requestUrl: request.url(),
            userRequest: {
                ip: request.ip()+" || "+request.ips(),
                hostname: request.hostname(),
                originalUrl: request.originalUrl(),
                method: request.method(),
                header: listRequestHeaders,
                body: request.all()
            },
            errorMessage: 'Header deviceid empty',
            errorName: 'Headers Unauthorized',
            errorStack: 'Header deviceid empty'
        }
      })

      return response.status(401).json(
        ResponseHelper({
            responseCode: 401,
            status: "false",
            message: 'Header deviceid empty',
            error: {
                message: 'Header deviceid empty'
            }
        })
      )
    }

    const getRedisGet = await Redis.get('deviceId'+listRequestHeaders['deviceid'])
    if(!getRedisGet){
      await Redis.set('deviceId'+listRequestHeaders['deviceid'], JSON.stringify({
        'deviceId':'deviceId'+listRequestHeaders['deviceid'],
        'deviceType':listRequestHeaders['devicetype'],
        'timestamps':moment().tz("Asia/Jakarta").format('x'),
        'countAccess':1
      }))
      await next();
      return false;
    }

    var getRedisData = JSON.parse(getRedisGet)
    const windowStartTimestamp = moment(getRedisData['timestamps'], 'x').tz("Asia/Jakarta").add(WINDOW_SIZE_IN_HOURS, 'hours').format('x');
    const thisTimestamps = moment().tz("Asia/Jakarta").format('x')

    if(thisTimestamps > windowStartTimestamp){
      getRedisData['timestamps'] = thisTimestamps,
      getRedisData['countAccess'] = 0;
    }

    if(getRedisData['countAccess'] > MAX_WINDOW_REQUEST_COUNT){
      await WriteTmpLog({
        directoryLog: 'error/',
        nameLog: 'error-log',
        fileData: {
            date: new Date(),
            requestUrl: request.url(),
            userRequest: {
                ip: request.ip()+" || "+request.ips(),
                hostname: request.hostname(),
                originalUrl: request.originalUrl(),
                method: request.method(),
                header: listRequestHeaders,
                body: request.all()
            },
            errorMessage: 'Request to many, your request is '+getRedisData['countAccess']+' max request '+MAX_WINDOW_REQUEST_COUNT+' every '+WINDOW_SIZE_IN_HOURS+' hours',
            errorName: 'Headers Unauthorized',
            errorStack: 'Request to many, your request is '+getRedisData['countAccess']+' max request '+MAX_WINDOW_REQUEST_COUNT+' every '+WINDOW_SIZE_IN_HOURS+' hours'
        }
      })

      return response.status(401).json(
        ResponseHelper({
            responseCode: 401,
            status: "false",
            message: 'Request to many, your request is '+getRedisData['countAccess']+' max request '+MAX_WINDOW_REQUEST_COUNT+' every '+WINDOW_SIZE_IN_HOURS+' hours',
            error: {
                message: 'Request to many, your request is '+getRedisData['countAccess']+' max request '+MAX_WINDOW_REQUEST_COUNT+' every '+WINDOW_SIZE_IN_HOURS+' hours'
            }
        })
      )
    }

    await Redis.set('deviceId'+listRequestHeaders['deviceid'], JSON.stringify({
      'deviceId':'deviceId'+listRequestHeaders['deviceid'],
      'deviceType':listRequestHeaders['devicetype'],
      'timestamps':getRedisData['timestamps'],
      'countAccess':getRedisData['countAccess'] + 1
    }))

    await next()
  }
}

module.exports = UserAgentAllow
