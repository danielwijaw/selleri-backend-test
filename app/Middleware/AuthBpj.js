'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ResponseHelperBpjs = require("./../Library/ResponseHelperBpjs");
const WriteTmpLog = require("./../Library/WriteTmpLog");
const User = use("App/Models/User");
const Redis = use('Redis');

class AuthBpj {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */

  async handle ({ request, response }, next) {

    if((!request.headers()['x-token'] && request.headers()['x-username']) || (!request.headers()['x-token'])){
      await WriteTmpLog({
        directoryLog: 'error/',
        nameLog: 'error-log',
        fileData: {
            date: new Date(),
            requestUrl: request.url(),
            userRequest: {
                ip: request.ip()+" || "+request.ips(),
                hostname: request.hostname(),
                originalUrl: request.originalUrl(),
                method: request.method(),
                header: {
                "User-Agent": request.header('User-Agent'),
                Host: request.header('User-Agent'),
                Authorization: request.header('Authorization'),
                "Content-Type": request.header('Content-Type'),
                Accept: request.header('Accept')
                },
                body: request.all()
            },
            errorMessage: "Headers x-token / x-username not found",
            errorName: 'Error Login',
            errorStack: "Headers x-token / x-username not found"
        }
      })
      return response.status(201).json(ResponseHelperBpjs({
        responseCode: 201,
        message: "Headers x-token / x-username not found",
        data: request.headers()
      }));
    }

    const userData = {
      username: request.headers()['x-username'],
      token: request.headers()['x-token']
    };
    const keyNameRedis = 'tokenAccountRequestBpjs '+userData.token+'_'+userData.username;
    const storeDataRedis = await Redis.get(keyNameRedis)
    if(storeDataRedis){
      await next();
      return true;
    }

    const userLogin = await User.query().where({users_username: userData.username, users_password: userData.token}).whereNull('deleted_at')
    .whereHas('employee_data').first()

    if(!userLogin){
      await WriteTmpLog({
        directoryLog: 'error/',
        nameLog: 'error-log',
        fileData: {
            date: new Date(),
            requestUrl: request.url(),
            userRequest: {
                ip: request.ip()+" || "+request.ips(),
                hostname: request.hostname(),
                originalUrl: request.originalUrl(),
                method: request.method(),
                header: {
                "User-Agent": request.header('User-Agent'),
                Host: request.header('User-Agent'),
                Authorization: request.header('Authorization'),
                "Content-Type": request.header('Content-Type'),
                Accept: request.header('Accept')
                },
                body: request.all()
            },
            errorMessage: "x-username / x-token wrong",
            errorName: 'Error Login',
            errorStack: "x-username / x-token wrong"
        }
      })
      return response.status(201).json(ResponseHelperBpjs({
        responseCode: 201,
        message: "x-username / x-token wrong",
        data: null
      }));
    }

    await Redis.set(keyNameRedis, JSON.stringify(userLogin.toJSON()))

    await next()
  }
}

module.exports = AuthBpj
