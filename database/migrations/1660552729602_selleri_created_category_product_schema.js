'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SelleriCreatedCategoryProductSchema extends Schema {
  up () {
    this.create('category_products', (table) => {
      table.uuid('category_products_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.string('category_products_name', 120).unique().notNullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('category_products')
  }
}

module.exports = SelleriCreatedCategoryProductSchema
