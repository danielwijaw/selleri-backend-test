'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SelleriCreatedCategoryProductTimestampsSchema extends Schema {
  async up () {
    return await this.db.raw(`
    CREATE TRIGGER category_products
    BEFORE UPDATE
    ON category_products
    FOR EACH ROW
    EXECUTE PROCEDURE upd_timestamp_users();
    `)
  }

  down () {
  }
}

module.exports = SelleriCreatedCategoryProductTimestampsSchema
