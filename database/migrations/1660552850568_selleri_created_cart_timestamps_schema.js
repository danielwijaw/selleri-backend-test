'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SelleriCreatedCartTimestampsSchema extends Schema {
  async up () {
    return await this.db.raw(`
    CREATE TRIGGER carts
    BEFORE UPDATE
    ON carts
    FOR EACH ROW
    EXECUTE PROCEDURE upd_timestamp_users();
    `)
  }

  down () {
  }
}

module.exports = SelleriCreatedCartTimestampsSchema
