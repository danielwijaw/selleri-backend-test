'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SelleriCreatedProductTimestampsSchema extends Schema {
  async up () {
    return await this.db.raw(`
    CREATE TRIGGER products
    BEFORE UPDATE
    ON products
    FOR EACH ROW
    EXECUTE PROCEDURE upd_timestamp_users();
    `)
  }

  down () {
  }
}

module.exports = SelleriCreatedProductTimestampsSchema
