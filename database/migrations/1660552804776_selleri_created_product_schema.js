'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SelleriCreatedProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.uuid('products_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.string('products_name', 120).notNullable()
      table.float('products_price').notNullable()
      table.string('products_location', 120).notNullable()
      table.text('products_detail').notNullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = SelleriCreatedProductSchema
