'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SelleriAddedFieldCategoryIdProductsSchema extends Schema {
  up () {
    this.table('products', (table) => {
      table.uuid('products_category_id').notNullable()
      // alter table
    })
  }

  down () {
    this.table('products', (table) => {
      table.dropColumn('products_category_id')
      // reverse alternations
    })
  }
}

module.exports = SelleriAddedFieldCategoryIdProductsSchema
