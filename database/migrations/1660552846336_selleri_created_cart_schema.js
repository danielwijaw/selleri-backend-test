'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SelleriCreatedCartSchema extends Schema {
  up () {
    this.create('carts', (table) => {
      table.uuid('carts_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.uuid('carts_category_product_id').notNullable()
      table.uuid('carts_product_id').notNullable()
      table.string('carts_consumen_name', 150).notNullable()
      table.float('carts_price').notNullable()
      table.json('carts_category_product_history').nullable()
      table.json('carts_product_history').nullable()
      table.string('carts_status', 15).notNullable().defaultTo('order')
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('carts')
  }
}

module.exports = SelleriCreatedCartSchema
