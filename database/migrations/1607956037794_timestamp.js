'use strict'

const Schema = use('Schema')
const Database = use('Database')

class SetupDbSchema extends Schema {
  async up () {
    await Database.raw(`
      CREATE OR REPLACE FUNCTION upd_timestamp_users() RETURNS TRIGGER 
      LANGUAGE plpgsql
      AS
      $$
      BEGIN
          NEW.updated_at = CURRENT_TIMESTAMP;
          RETURN NEW;
      END;
      $$;

      CREATE TRIGGER users
        BEFORE UPDATE
        ON users
        FOR EACH ROW
        EXECUTE PROCEDURE upd_timestamp_users();
    `)
    return await Database.raw(`
      CREATE OR REPLACE FUNCTION upd_timestamp_tokens() RETURNS TRIGGER 
      LANGUAGE plpgsql
      AS
      $$
      BEGIN
          NEW.updated_at = CURRENT_TIMESTAMP;
          RETURN NEW;
      END;
      $$;

      CREATE TRIGGER tokens
        BEFORE UPDATE
        ON tokens
        FOR EACH ROW
        EXECUTE PROCEDURE upd_timestamp_tokens();
    `)
  }

  down () {
    
  }
}

module.exports = SetupDbSchema