'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const ResponseHelper = require("../app/Library/ResponseHelper")
const metadataVersion = require("./../metadata.json")

Route.group(() => {
  Route.get("personal", "EmployeeController.personalEmployee");

  Route.resource("employee-report", "TransactionEmployeeReportController").apiOnly();
  Route.post("employee-type-positions", "EmployeeTypePositionsController.setHeadPositions");
  Route.get("employee-type-positions", "EmployeeTypePositionsController.getStructural");
  Route.resource("employee-type-position", "EmployeeTypePositionsController").apiOnly();
  Route.get("employee-type", "AdditionalDataController.employeeType");
  Route.resource("employee-status", "EmployeeStatusController").apiOnly();
  Route.resource("employee-administartion-types", "EmployeeAdministrationsTypeController").apiOnly();
  Route.resource("employee", "EmployeeController").apiOnly();
  Route.resource("employee-contracts", "EmployeeContractController").apiOnly();
  Route.get("employee-administrations/data/:id", "EmployeeAdministrationController.showData");
  Route.resource("employee-administrations", "EmployeeAdministrationController").apiOnly();
  Route.resource("employee-departments", "EmployeeDepartmentController").apiOnly();
  Route.resource("employee-positions", "EmployeePositionController").apiOnly();
  Route.resource("employee-vacation", "Employee/EmployeeVacationController").apiOnly();

  Route.resource("employee-schedule", "Employee/ScheduleController").apiOnly();
  Route.resource("employee-attendance", "Employee/AttendanceController").apiOnly();
  Route.post("employee-barcode", "Employee/AttendanceController.barcode");

  Route.post("employee-resign/:id", "EmployeeController.setEmployeeResign");
  Route.post("employee-unresign/:id", "EmployeeController.setEmployeeUnResign");

  Route.resource("employee-training-request", "Employee/SkillTrainingController").apiOnly();

  Route.get("employee-profile-picture/data/:id", "Employee/ProfilePictureController.showData");
  Route.resource("employee-profile-picture", "Employee/ProfilePictureController").apiOnly();

  Route.resource("employee-sallary", "Employee/SallaryController").apiOnly();
  Route.post("employee-sallary-generate", "Employee/SallaryController.generate");

  Route.resource("department", "DepartmentController").apiOnly();
  Route.resource("department-type", "DepartmentTypeController").apiOnly();

  Route.get("notifications-type", "Log/NotificationController.listTypeNotifications");

  Route.resource("notifications", "Log/NotificationController");

  Route.resource("app-version", "Frontend/AppVersionController");

}).prefix("api/v1").middleware(["auth", "userAgent"]);

Route.group(() => {

  Route.get("employee-profile-picture/data/:id", "Employee/ProfilePictureController.showData");

  Route.post("register", "UserController.register").middleware(["auth", "userAgent"]);

  Route.post("register-guest", "UserController.register").middleware("guest");

  Route.post("login", "UserController.login").middleware("guest");
  Route.patch("change-password", "UserController.updatePassword").middleware(["auth", "userAgent"]);
  Route.patch("change-username", "UserController.updateUsername").middleware(["auth", "userAgent"]);
  Route.post("logout", "UserController.logout").middleware(["auth", "userAgent"]);

  Route.post("read-log", "Log/HistoryRecordController.checkHistoryCreated").middleware(["auth", "userAgent"]);
  Route.post("read-log-created", "Log/HistoryRecordController.checkHistoryCreated").middleware(["auth", "userAgent"]);
  Route.post("read-log-removed", "Log/HistoryRecordController.checkHistoryRemoved").middleware(["auth", "userAgent"]);
  Route.post("read-log-return-patient", "Log/HistoryRecordController.checkHistoryReturnPatient").middleware(["auth", "userAgent"]);
  Route.post("read-log-activity", "Log/HistoryRecordController.checkHistoryConfirm").middleware(["auth", "userAgent"]);

  Route.post("reset-password", "UserController.resetPassword").middleware(["auth", "userAgent"]);

  Route.post("employee-generate-format", "Employee/ShiftSettingController.excelFormat");
  Route.get("employee-generate-format", "Employee/ShiftSettingController.excelFormat");

  Route.post("employee-import-shift", "Employee/ShiftSettingController.importExcel");

  Route.resource("frontend-menus", "Frontend/MenuController").apiOnly().middleware(["auth", "userAgent"]);
  Route.post("menus-right-user", "Frontend/MenuController.checkMenusUser").middleware(["auth", "userAgent"]);
  Route.resource("menus-right", "Frontend/RightController").apiOnly().middleware(["auth", "userAgent"]);

  Route.post("clear-redis", "Frontend/StatusActivateController.clearCacheRedis").middleware(["auth", "userAgent"]);
  Route.get("clear-redis", "Frontend/StatusActivateController.clearCacheRedis")

  Route.get("attendance-barcode/:id", "EmployeeController.getQrAttendance");
  Route.post("employee-barcode", "Employee/AttendanceController.barcode");

}).prefix("api/v1/users")

Route.group(() => {
  Route.get("gender", "AdditionalDataController.gender");
  Route.get("religion", "AdditionalDataController.religion");
  Route.get("marriage-status", "AdditionalDataController.marriageStatus");
  Route.get("national-status", "AdditionalDataController.nationalStatus");
  Route.get("patient-title", "AdditionalDataController.patientTitle");
  Route.get("college", "AdditionalDataController.college");
  Route.get("card-list", "AdditionalDataController.cardList");
  Route.resource("profession", "AdditionalDataProfessionController").apiOnly();
  Route.get("frequently-languange", "AdditionalDataController.frequentlyLanguange");
  Route.resource("province", "ProvinceController").apiOnly();
  Route.resource("district", "DistrictController").apiOnly();
  Route.resource("sub-district", "SubDistrictController").apiOnly();
  Route.resource("village", "VillageController").apiOnly();
}).prefix("api/v1/additional-data").middleware(["auth", "userAgent"]);

Route.group(() => {
  Route.resource("category-product", "Selleri/CategoryProductController").apiOnly();
  Route.resource("product", "Selleri/ProductController").apiOnly();
  Route.resource("cart", "Selleri/CartController").apiOnly();
}).prefix("api/v1/selleri");

Route.group(() => {
  Route.get("information", "AdditionalDataController.websiteInformation")
  Route.get("information/:id", "AdditionalDataController.websiteInformationRow")

  Route.resource("employee-status", "EmployeeStatusController").apiOnly();
  Route.resource("employee", "EmployeeController").apiOnly();

  Route.get("log", "LogController.listDirectory")
  Route.get("logread", "LogController.readFile")

}).prefix('api/-_-')

Route.get('*', () => {
  return ResponseHelper({
    message: "SIMS",
    data: {
      codename: "Siel Studio",
      version: metadataVersion.buildMajor+"."+metadataVersion.buildMinor+"."+metadataVersion.buildRevision
    }
  })
})
